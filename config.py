"""
Oauth configuration data
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

import os

DEBUG_MODE = os.getenv('DEBUG_MODE', False)
TOKEN_ENCRYPT_SECRET_KEY = os.getenv('TOKEN_ENCRYPT_SECRET_KEY')
SECRET_KEY = os.getenv('SECRET_KEY')
GITHUB_CLIENT_ID = os.getenv('GITHUB_CLIENT_ID')
GITHUB_CLIENT_SECRET = os.getenv('GITHUB_CLIENT_SECRET')
SESSION_EXPIRATION_TIME = os.getenv('SESSION_EXPIRATION_TIME')

MONGO_HOST = os.getenv('MONGO_HOST', 'localhost')
MONGO_PORT = os.getenv('MONGO_PORT', 27017)
MONGO_DB = os.getenv('MONGO_DB', 'test')
MONGO_USER = os.getenv('MONGO_USER', 'root')
MONGO_PASS = os.getenv('MONGO_PASS', 'root')

REDIS_HOST = os.getenv('REDIS_HOST', 'localhost')
REDIS_PORT = os.getenv('REDIS_PORT', 6379)
REDIS_PASS = os.getenv('REDIS_PASS', 'root')
