"""
Unitary tests for crm_api/services/customer_service.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-11
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.services import customer_service

class TestCustomerService(unittest.TestCase):
    """
    Unitary tests for CustomerService.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        self._db_connector = mock.Mock()
        self._serv = customer_service.CustomerService(self._db_connector)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    def test_get_customers(self):
        self._db_connector.get_customers.return_value = ['customer']
        customer = self._serv.get_customers()
        self._db_connector.get_customers.assert_called_once_with()
        self.assertListEqual(customer, ['customer'])

    def test_get_customer_by_id(self):
        self._db_connector.get_customer_by_id.return_value = 'customer'
        customer = self._serv.get_customer_by_id('1234')
        self._db_connector.get_customer_by_id.assert_called_once_with('1234')
        self.assertEqual(customer, 'customer')

    def test_get_customer_by_id_error(self):
        self._db_connector.get_customer_by_id.side_effect = \
            customer_service.customer_repository.CustomerNotExistError('error')

        with self.assertRaises(customer_service.CustomerNotExistError) as err:
            self._serv.get_customer_by_id('1234')
            self._db_connector.get_customer_by_id.assert_called_once_with('1234')
            self.assertTrue('error' in str(err.exception))

    def test_create_customer(self):
        self._db_connector.create_customer.return_value = 'customer'
        customer = self._serv.create_customer('name', 'surname', 'user', 'photo')
        self._db_connector.create_customer.assert_called_once_with(
            'name', 'surname', 'user', 'photo')
        self.assertEqual(customer, 'customer')

    def test_create_customer_error(self):
        self._db_connector.create_customer.side_effect = \
            customer_service.customer_repository.InvalidDataError('error')

        with self.assertRaises(customer_service.InvalidDataError) as err:
            self._serv.create_customer('name', 'surname', 'user', 'photo')
            self._db_connector.create_customer.assert_called_once_with(
                'name', 'surname', 'user', 'photo')
            self.assertTrue('error' in str(err.exception))

    def test_update_customer(self):
        self._db_connector.update_customer.return_value = 'customer'
        customer = self._serv.update_customer('id', 'name', 'surname', 'user', 'photo')
        self._db_connector.update_customer.assert_called_once_with(
            'id', 'name', 'surname', 'user', 'photo')
        self.assertEqual(customer, 'customer')

    def test_update_customer_error(self):
        self._db_connector.update_customer.side_effect = \
            customer_service.customer_repository.InvalidDataError('error')

        with self.assertRaises(customer_service.InvalidDataError) as err:
            self._serv.update_customer('id', 'name', 'surname', 'user', 'photo')
            self._db_connector.update_customer.assert_called_once_with(
                'id', 'name', 'surname', 'user', 'photo')
            self.assertTrue('error' in str(err.exception))

    def test_update_customer_error2(self):
        self._db_connector.update_customer.side_effect = \
            customer_service.customer_repository.CustomerNotExistError('error')

        with self.assertRaises(customer_service.CustomerNotExistError) as err:
            self._serv.update_customer('id', 'name', 'surname', 'user', 'photo')
            self._db_connector.update_customer.assert_called_once_with(
                'id', 'name', 'surname', 'user', 'photo')
            self.assertTrue('error' in str(err.exception))

    def test_delete_customer(self):
        self._db_connector.delete_customer.return_value = 'customer'
        self._serv.delete_customer('1234')
        self._db_connector.delete_customer.assert_called_once_with('1234')

    def test_delete_customer_error(self):
        self._db_connector.delete_customer.side_effect = \
            customer_service.customer_repository.CustomerNotExistError('error')

        with self.assertRaises(customer_service.CustomerNotExistError) as err:
            self._serv.delete_customer('1234')
            self._db_connector.delete_customer.assert_called_once_with('1234')
            self.assertTrue('error' in str(err.exception))
