"""
Unitary tests for crm_api/services/user_service.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-11
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.services import user_service

class TestUserService(unittest.TestCase):
    """
    Unitary tests for UserService.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        self._db_connector = mock.Mock()
        self._serv = user_service.UserService(self._db_connector)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    def test_get_users(self):
        self._db_connector.get_users.return_value = ['user']
        user = self._serv.get_users()
        self._db_connector.get_users.assert_called_once_with()
        self.assertListEqual(user, ['user'])

    def test_get_user_by_username(self):
        self._db_connector.get_user_by_username.return_value = 'user'
        user = self._serv.get_user_by_username('1234')
        self._db_connector.get_user_by_username.assert_called_once_with('1234')
        self.assertEqual(user, 'user')

    def test_get_user_by_username_error(self):
        self._db_connector.get_user_by_username.side_effect = \
            user_service.user_repository.UserNotExistError('error')

        with self.assertRaises(user_service.UserNotExistError) as err:
            self._serv.get_user_by_username('1234')
            self._db_connector.get_user_by_username.assert_called_once_with('1234')
            self.assertTrue('error' in str(err.exception))

    def test_get_user_by_email(self):
        self._db_connector.get_user_by_email.return_value = 'user'
        user = self._serv.get_user_by_email('1234')
        self._db_connector.get_user_by_email.assert_called_once_with('1234')
        self.assertEqual(user, 'user')

    def test_get_user_by_email_error(self):
        self._db_connector.get_user_by_email.side_effect = \
            user_service.user_repository.UserNotExistError('error')

        with self.assertRaises(user_service.UserNotExistError) as err:
            self._serv.get_user_by_email('1234')
            self._db_connector.get_user_by_email.assert_called_once_with('1234')
            self.assertTrue('error' in str(err.exception))

    def test_create_user(self):
        self._db_connector.create_user.return_value = 'user'
        user = self._serv.create_user('username', 'name', 'maile', False)
        self._db_connector.create_user.assert_called_once_with(
            'username', 'name', 'maile', False)
        self.assertEqual(user, 'user')

    def test_create_user_error(self):
        self._db_connector.create_user.side_effect = \
            user_service.user_repository.InvalidDataError('error')

        with self.assertRaises(user_service.InvalidDataError) as err:
            self._serv.create_user('username', 'name', 'maile', False)
            self._db_connector.create_user.assert_called_once_with(
                'username', 'name', 'maile', False)
            self.assertTrue('error' in str(err.exception))

    def test_update_user_superadmin(self):
        self._serv.get_user_by_username = mock.Mock()
        self._serv.get_user_by_username.return_value = {'is_super_admin': True}
        with self.assertRaises(user_service.SuperAdminError) as err:
            self._serv.update_user('username', 'name', 'maile', False)
            self.assertTrue(
                'superadmin users cannot be updated' in str(err.exception))

    def test_update_user(self):
        self._serv.get_user_by_username = mock.Mock()
        self._serv.get_user_by_username.return_value = {'is_super_admin': False}
        self._db_connector.update_user.return_value = 'user'
        user = self._serv.update_user('username', 'name', 'maile', False)
        self._db_connector.update_user.assert_called_once_with(
            'username', 'name', 'maile', False)
        self.assertEqual(user, 'user')

    def test_update_user_error(self):
        self._serv.get_user_by_username = mock.Mock()
        self._serv.get_user_by_username.return_value = {'is_super_admin': False}
        self._db_connector.update_user.side_effect = \
            user_service.user_repository.InvalidDataError('error')

        with self.assertRaises(user_service.InvalidDataError) as err:
            self._serv.update_user('username', 'name', 'maile', False)
            self._db_connector.update_user.assert_called_once_with(
                'username', 'name', 'maile', False)
            self.assertTrue('error' in str(err.exception))

    def test_update_user_error2(self):
        self._serv.get_user_by_username = mock.Mock()
        self._serv.get_user_by_username.return_value = {'is_super_admin': False}
        self._db_connector.update_user.side_effect = \
            user_service.user_repository.UserNotExistError('error')

        with self.assertRaises(user_service.UserNotExistError) as err:
            self._serv.update_user('username', 'name', 'maile', False)
            self._db_connector.update_user.assert_called_once_with(
                'username', 'name', 'maile', False)
            self.assertTrue('error' in str(err.exception))

    def test_delete_superadmin(self):
        self._serv.get_user_by_username = mock.Mock()
        self._serv.get_user_by_username.return_value = {'is_super_admin': True}
        with self.assertRaises(user_service.SuperAdminError) as err:
            self._serv.delete_user('username')
            self.assertTrue(
                'superadmin users cannot be deleted' in str(err.exception))

    def test_delete_user(self):
        self._serv.get_user_by_username = mock.Mock()
        self._serv.get_user_by_username.return_value = {'is_super_admin': False}
        self._db_connector.delete_user.return_value = 'user'
        self._serv.delete_user('1234')
        self._db_connector.delete_user.assert_called_once_with('1234')

    def test_delete_user_error(self):
        self._serv.get_user_by_username = mock.Mock()
        self._serv.get_user_by_username.return_value = {'is_super_admin': False}
        self._db_connector.delete_user.side_effect = \
            user_service.user_repository.UserNotExistError('error')

        with self.assertRaises(user_service.UserNotExistError) as err:
            self._serv.delete_user('1234')
            self._db_connector.delete_user.assert_called_once_with('1234')
            self.assertTrue('error' in str(err.exception))
