"""
Unitary tests for crm_api/authorization/oauth.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-12
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.authorization import oauth

class TestOauth(unittest.TestCase):
    """
    Unitary tests for authorization ouath.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    @mock.patch('crm_api.authorization.oauth.OAuth')
    @mock.patch('crm_api.authorization.oauth.create_flask_blueprint')
    @mock.patch('crm_api.authorization.oauth.GitHub')
    def setUp(self, mock_github, mock_blueprint, mock_oauth):
        """
        Test setUp.
        """
        self._mock_app = mock.Mock()
        self._mock_cache = mock.Mock()
        self._mock_service = mock.Mock()
        mock_blueprint.return_value = 'blueprint'
        mock_oauth.return_value = 'Oauth'
        self._oauth = oauth.Authorization(
            self._mock_app, self._mock_cache, self._mock_service, '/', 60)
        mock_oauth.assert_called_once_with(self._mock_app, cache=self._mock_cache)
        mock_blueprint.assert_called_once_with(
            mock_github, 'Oauth', self._oauth._handle_authorize)
        self._mock_app.register_blueprint.assert_called_once_with('blueprint', url_prefix='/')

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.controllers.utils.encode_token')
    @mock.patch('crm_api.controllers.utils.format_token')
    @mock.patch('crm_api.authorization.oauth.jsonify')
    def test_handle(self, mock_jsonify, mock_format, mock_encode):
        user_info = {'preferred_username': 'user', 'email': 'email'}
        mock_encode.return_value = 'encoded_token'
        mock_format.return_value = 'formatted_token'
        self._mock_service.get_user_by_username.return_value = 'username'
        mock_jsonify.return_value = 'response'

        response = self._oauth._handle_authorize(
            'remote', {'access_token': 'token'}, user_info)

        mock_encode.assert_called_once_with('token')
        self._mock_cache.set.assert_called_once_with('encoded_token', 'user', ex=60)
        mock_format.assert_called_once_with('token')
        self._mock_service.get_user_by_username.assert_called_once_with('user')
        mock_jsonify.assert_called_once_with(
            {'message': 'Successfully logged.',
             'auth_token': 'formatted_token'})

        self.assertEqual(response, ('response', 200))

    @mock.patch('crm_api.controllers.utils.encode_token')
    @mock.patch('crm_api.controllers.utils.format_token')
    @mock.patch('crm_api.authorization.oauth.jsonify')
    def test_handle_email(self, mock_jsonify, mock_format, mock_encode):
        user_info = {'preferred_username': 'user', 'email': 'email'}
        mock_encode.return_value = 'encoded_token'
        mock_format.return_value = 'formatted_token'
        self._mock_service.get_user_by_username.side_effect = oauth.UserNotExistError('error')
        mock_jsonify.return_value = 'response'

        response = self._oauth._handle_authorize(
            'remote', {'access_token': 'token'}, user_info)

        mock_encode.assert_called_once_with('token')
        self._mock_cache.set.assert_called_once_with('encoded_token', 'user', ex=60)
        mock_format.assert_called_once_with('token')
        self._mock_service.get_user_by_username.assert_called_once_with('user')
        self._mock_service.get_user_by_email.assert_called_once_with('email')
        mock_jsonify.assert_called_once_with(
            {'message': 'Successfully logged.',
             'auth_token': 'formatted_token'})

        self.assertEqual(response, ('response', 200))

    @mock.patch('crm_api.authorization.oauth.abort')
    def test_handle_no_token(self, mock_abort):
        self._oauth._handle_authorize('remote', None, {})
        mock_abort.assert_called_once_with(401, message='authorization failed')

    @mock.patch('crm_api.controllers.utils.encode_token')
    @mock.patch('crm_api.controllers.utils.format_token')
    @mock.patch('crm_api.authorization.oauth.abort')
    def test_handle_no_user(self, mock_abort, mock_format, mock_encode):
        mock_encode.return_value = 'encoded_token'
        mock_format.return_value = 'formatted_token'
        self._mock_service.get_user_by_username.return_value = 'username'
        self._oauth._handle_authorize('remote', None, {})
        mock_abort.assert_called_once_with(401, message='authorization failed')
        mock_abort.assert_called_once_with(401, message='authorization failed')

    @mock.patch('crm_api.controllers.utils.encode_token')
    @mock.patch('crm_api.controllers.utils.format_token')
    @mock.patch('crm_api.authorization.oauth.jsonify')
    def test_handle_register(self, mock_jsonify, mock_format, mock_encode):
        user_info = {'preferred_username': 'user', 'email': 'email', 'name': 'name'}
        mock_encode.return_value = 'encoded_token'
        mock_format.return_value = 'formatted_token'
        self._mock_service.get_user_by_username.side_effect = oauth.UserNotExistError('error')
        self._mock_service.get_user_by_email.side_effect = oauth.UserNotExistError('error')
        mock_jsonify.return_value = 'response'

        response = self._oauth._handle_authorize(
            'remote', {'access_token': 'token'}, user_info)

        mock_encode.assert_called_once_with('token')
        self._mock_cache.set.assert_called_once_with('encoded_token', 'user', ex=60)
        mock_format.assert_called_once_with('token')
        self._mock_service.get_user_by_username.assert_called_once_with('user')
        self._mock_service.get_user_by_email.assert_called_once_with('email')
        self._mock_service.create_user.assert_called_once_with(
            username='user',
            name='name',
            email='email',
            is_admin=False
        )
        mock_jsonify.assert_called_once_with(
            {'message': 'Successfully registered.',
             'auth_token': 'formatted_token'})

        self.assertEqual(response, ('response', 201))
