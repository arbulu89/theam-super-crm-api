"""
Unitary tests for crm_api/repositories/mongodb/user_repository.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-12
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.repositories import user_repository as base_user_repository
from crm_api.repositories.mongodb import user_repository

class TestMongodbRepository(unittest.TestCase):
    """
    Unitary tests for UserRepository.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        self._repo = user_repository.MongodbRepository()

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.repositories.mongodb.utils.connect')
    def test_connect(self, mock_connect):
        mock_connect.return_value = 'connected'
        self._repo.connect('db', 'username', 'password', host='host')
        self.assertEqual(self._repo._connection, 'connected')
        mock_connect.assert_called_once_with('db', 'username', 'password', host='host')

    def test_disconnect(self):
        self._repo._connection = mock.Mock()
        self._repo.disconnect()
        self._repo._connection.close.assert_called_once_with()

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_get_users(self, mock_to_dict, mock_user):
        mock_to_dict.side_effect = ['user_dict1', 'user_dict2']
        mock_user.objects = ['user1', 'user2']
        users = self._repo.get_users()
        self.assertListEqual(['user_dict1', 'user_dict2'], users)
        mock_to_dict.assert_has_calls([
            mock.call('user1'),
            mock.call('user2')
        ])

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_get_user_by_username(self, mock_to_dict, mock_user):
        mock_user.objects.return_value = ['user1']
        mock_to_dict.return_value = 'user_dict1'
        user = self._repo.get_user_by_username('username')
        self.assertEqual(user, 'user_dict1')
        mock_user.objects.assert_called_once_with(username='username')

    @mock.patch('crm_api.models.user_model.User')
    def test_get_user_by_username_error(self, mock_user):
        mock_user.objects.return_value = []
        with self.assertRaises(base_user_repository.UserNotExistError) as err:
            self._repo.get_user_by_username('username')
            mock_user.objects.assert_called_once_with(username='username')
            self.assertTrue(
                'User with username {} does not exist'.format('username') in err.exception)

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_get_user_by_email(self, mock_to_dict, mock_user):
        mock_user.objects.return_value = ['user1']
        mock_to_dict.return_value = 'user_dict1'
        user = self._repo.get_user_by_email('email')
        self.assertEqual(user, 'user_dict1')
        mock_user.objects.assert_called_once_with(email='email')

    @mock.patch('crm_api.models.user_model.User')
    def test_get_user_by_email_error(self, mock_user):
        mock_user.objects.return_value = []
        with self.assertRaises(base_user_repository.UserNotExistError) as err:
            self._repo.get_user_by_email('email')
            mock_user.objects.assert_called_once_with(email='email')
            self.assertTrue(
                'User with email {} does not exist'.format('email') in err.exception)

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_create_user(self, mock_to_dict, mock_user):
        new_user = mock.Mock()
        mock_user.return_value = new_user
        mock_to_dict.return_value = 'user1_created'
        user = self._repo.create_user('username', 'name', 'email', True, True)

        mock_user.assert_called_once_with(
            username='username', name='name', email='email',
            is_admin=True, is_super_admin=True)
        new_user.save.assert_called_once_with()

        mock_to_dict.assert_called_once_with(new_user)
        self.assertEqual(user, 'user1_created')

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.user_repository.mongoengine')
    def test_create_user_error(self, mock_engine, mock_user):
        mock_engine.errors.ValidationError = Exception
        mock_user.side_effect = mock_engine.errors.ValidationError('error')
        with self.assertRaises(base_user_repository.InvalidDataError) as err:
            self._repo.create_user('username', 'name', 'email', True, True)
            self.assertTrue('Provided user data is not valid: {}'.format('error') in err.exception)

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.user_repository.mongoengine')
    def test_create_user_unique_error(self, mock_engine, mock_user):
        mock_engine.errors.NotUniqueError = Exception
        mock_user.side_effect = mock_engine.errors.NotUniqueError('error')
        with self.assertRaises(base_user_repository.InvalidDataError) as err:
            self._repo.create_user('username', 'name', 'email', True, True)
            self.assertTrue(
                'Provided unique data already exists: {}'.format('error') in err.exception)

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_update_user(self, mock_to_dict, mock_user):
        new_user = mock.Mock()
        mock_user.objects.return_value = [new_user]
        mock_to_dict.return_value = 'user1_updated'
        user = self._repo.update_user(
            'username', 'name', 'email', True)
        self.assertEqual(user, 'user1_updated')
        mock_user.objects.assert_called_once_with(username='username')
        new_user.save.assert_called_once_with()
        mock_to_dict.assert_called_once_with(new_user)
        self.assertEqual(new_user.username, 'username')
        self.assertEqual(new_user.name, 'name')
        self.assertEqual(new_user.email, 'email')
        self.assertEqual(new_user.is_admin, True)
        self.assertEqual(new_user.is_super_admin, False)

    @mock.patch('crm_api.models.user_model.User')
    def test_update_user_not_exist(self, mock_user):
        mock_user.objects.return_value = []
        with self.assertRaises(base_user_repository.UserNotExistError) as err:
            self._repo.update_user('username', 'name', 'email', True)
            self.assertTrue('User with id {} does not exist'.format('id') in err.exception)

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.user_repository.mongoengine')
    def test_update_user_validation_error(self, mock_engine, mock_user):
        mock_engine.errors.ValidationError = Exception
        new_user = mock.Mock()
        new_user.save.side_effect = mock_engine.errors.ValidationError('error')
        mock_user.objects.return_value = [new_user]

        with self.assertRaises(base_user_repository.InvalidDataError) as err:
            self._repo.update_user('username', 'name', 'email', True)
            self.assertTrue('Provided user data is not valid: {}'.format('error') in err.exception)

    @mock.patch('crm_api.models.user_model.User')
    @mock.patch('crm_api.repositories.mongodb.user_repository.mongoengine')
    def test_update_user_duplicated_error(self, mock_engine, mock_user):
        mock_engine.errors.NotUniqueError = Exception
        new_user = mock.Mock()
        new_user.save.side_effect = mock_engine.errors.NotUniqueError('error')
        mock_user.objects.return_value = [new_user]

        with self.assertRaises(base_user_repository.InvalidDataError) as err:
            self._repo.update_user('username', 'name', 'email', True)
            self.assertTrue(
                'Provided unique data already exists: {}'.format('error') in err.exception)

    @mock.patch('crm_api.models.user_model.User')
    def test_delete_user(self, mock_user):
        delete_user = mock.Mock()
        mock_user.objects.return_value = [delete_user]
        self._repo.delete_user('username')

        mock_user.objects.assert_called_once_with(username='username')
        delete_user.delete.assert_called_once_with()

    @mock.patch('crm_api.models.user_model.User')
    def test_delete_user_error(self, mock_user):
        mock_user.objects.return_value = []
        with self.assertRaises(base_user_repository.UserNotExistError) as err:
            self._repo.delete_user('username')
            mock_user.objects.assert_called_once_with(username='username')
            self.assertTrue('User with username {} does not exist'.format('username') in err.exception)
