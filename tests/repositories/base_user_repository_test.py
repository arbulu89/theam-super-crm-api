"""
Unitary tests for crm_api/repositories/user_repository.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-11
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.repositories import user_repository

class TestUserRepository(unittest.TestCase):
    """
    Unitary tests for UserRepository.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        self._repo = user_repository.BaseRepository()

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    def test_get_users(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.get_users()
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_get_user_by_username(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.get_user_by_username('id')
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_get_user_by_email(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.get_user_by_email('my-mail')
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_create_user(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.create_user('username', 'name', 'email', True)
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_update_user(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.update_user('username', 'name', 'email', True)
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_delete_user(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.delete_user('id')
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))
