"""
Unitary tests for crm_api/repositories/mongodb/customer_repository.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-12
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.repositories import customer_repository as base_customer_repository
from crm_api.repositories.mongodb import customer_repository

class TestMongodbRepository(unittest.TestCase):
    """
    Unitary tests for UserRepository.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        self._repo = customer_repository.MongodbRepository()

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.repositories.mongodb.utils.connect')
    def test_connect(self, mock_connect):
        mock_connect.return_value = 'connected'
        self._repo.connect('db', 'username', 'password', host='host')
        self.assertEqual(self._repo._connection, 'connected')
        mock_connect.assert_called_once_with('db', 'username', 'password', host='host')

    def test_disconnect(self):
        self._repo._connection = mock.Mock()
        self._repo.disconnect()
        self._repo._connection.close.assert_called_once_with()

    @mock.patch('crm_api.models.customer_model.Customer')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_get_customers(self, mock_to_dict, mock_customer):
        mock_to_dict.side_effect = ['customer_dict1', 'customer_dict2']
        mock_customer.objects = ['customer1', 'customer2']
        customers = self._repo.get_customers()
        self.assertListEqual(['customer_dict1', 'customer_dict2'], customers)
        mock_to_dict.assert_has_calls([
            mock.call('customer1'),
            mock.call('customer2')
        ])

    @mock.patch('crm_api.models.customer_model.Customer')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_get_customer_by_id(self, mock_to_dict, mock_customer):
        mock_customer.objects.return_value = ['customer1']
        mock_to_dict.return_value = 'customer_dict1'
        customer = self._repo.get_customer_by_id('id')
        self.assertEqual(customer, 'customer_dict1')
        mock_customer.objects.assert_called_once_with(id='id')

    @mock.patch('crm_api.models.customer_model.Customer')
    def test_get_customer_by_id_error(self, mock_customer):
        mock_customer.objects.return_value = []
        with self.assertRaises(base_customer_repository.CustomerNotExistError) as err:
            self._repo.get_customer_by_id('id')
            mock_customer.objects.assert_called_once_with(id='id')
            self.assertTrue('Customer with id {} does not exist'.format('id') in err.exception)

    @mock.patch('crm_api.models.customer_model.Customer')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_create_customer(self, mock_to_dict, mock_customer):
        new_customer = mock.Mock()
        mock_customer.return_value = new_customer
        mock_to_dict.return_value = 'customer1_created'
        customer = self._repo.create_customer('name', 'surname', 'user', 'photo')

        mock_customer.assert_called_once_with(
            name='name', surname='surname', created_by='user', photo='photo')
        new_customer.save.assert_called_once_with()

        mock_to_dict.assert_called_once_with(new_customer)
        self.assertEqual(customer, 'customer1_created')

    @mock.patch('crm_api.models.customer_model.Customer')
    @mock.patch('crm_api.repositories.mongodb.customer_repository.mongoengine')
    def test_create_customer_error(self, mock_engine, mock_customer):
        mock_engine.errors.ValidationError = Exception
        mock_customer.side_effect = mock_engine.errors.ValidationError('error')
        with self.assertRaises(base_customer_repository.InvalidDataError) as err:
            self._repo.create_customer('name', 'surname', 'user', 'photo')
            self.assertTrue('Provided user data is not valid: {}'.format('error') in err.exception)

    @mock.patch('crm_api.models.customer_model.Customer')
    @mock.patch('crm_api.repositories.mongodb.utils.mongo_to_dict')
    def test_update_customer(self, mock_to_dict, mock_customer):
        new_customer = mock.Mock()
        mock_customer.objects.return_value = [new_customer]
        mock_to_dict.return_value = 'customer1_updated'
        customer = self._repo.update_customer(
            'id', 'name', 'surname', 'user', 'photo')
        self.assertEqual(customer, 'customer1_updated')
        mock_customer.objects.assert_called_once_with(id='id')
        new_customer.save.assert_called_once_with()
        mock_to_dict.assert_called_once_with(new_customer)
        self.assertEqual(new_customer.name, 'name')
        self.assertEqual(new_customer.surname, 'surname')
        self.assertEqual(new_customer.updated_by, 'user')
        self.assertEqual(new_customer.photo, 'photo')

    def test_update_customer_invalid(self):
        with self.assertRaises(base_customer_repository.InvalidDataError) as err:
            self._repo.update_customer('id', 'name', 'surname', None, 'photo')
            self.assertTrue(
                'Provided user data is not valid: ValidationError (Customer:None)'
                '(Required parameter missing: [\'updated_by\'])' in err.exception)

    @mock.patch('crm_api.models.customer_model.Customer')
    def test_update_customer_not_exist(self, mock_customer):
        mock_customer.objects.return_value = []
        with self.assertRaises(base_customer_repository.CustomerNotExistError) as err:
            self._repo.update_customer('id', 'name', 'surname', 'user', 'photo')
            self.assertTrue('Customer with id {} does not exist'.format('id') in err.exception)

    @mock.patch('crm_api.models.customer_model.Customer')
    @mock.patch('crm_api.repositories.mongodb.customer_repository.mongoengine')
    def test_update_customer_validation_error(self, mock_engine, mock_customer):
        mock_engine.errors.ValidationError = Exception
        new_customer = mock.Mock()
        new_customer.save.side_effect = mock_engine.errors.ValidationError('error')
        mock_customer.objects.return_value = [new_customer]

        with self.assertRaises(base_customer_repository.InvalidDataError) as err:
            self._repo.update_customer('id', 'name', 'surname', 'user', 'photo')
            self.assertTrue('Provided user data is not valid: {}'.format('error') in err.exception)

    @mock.patch('crm_api.models.customer_model.Customer')
    def test_delete_customer(self, mock_customer):
        delete_customer = mock.Mock()
        mock_customer.objects.return_value = [delete_customer]
        self._repo.delete_customer('id')

        mock_customer.objects.assert_called_once_with(id='id')
        delete_customer.delete.assert_called_once_with()

    @mock.patch('crm_api.models.customer_model.Customer')
    def test_delete_customer_error(self, mock_customer):
        mock_customer.objects.return_value = []
        with self.assertRaises(base_customer_repository.CustomerNotExistError) as err:
            self._repo.delete_customer('id')
            mock_customer.objects.assert_called_once_with(id='id')
            self.assertTrue('Customer with id {} does not exist'.format('id') in err.exception)
