"""
Unitary tests for crm_api/repositories/mongodb/utils.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-11
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.repositories.mongodb import utils

class TestMongoDBUtils(unittest.TestCase):
    """
    Unitary tests for utils.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    def test_mongo_to_dict(self):

        initial = mock.Mock()
        initial.to_mongo.return_value = {
            '_id': 1234,
            'data': 'data',
            'data2': 'data2'
        }
        converted = utils.mongo_to_dict(initial)
        self.assertDictEqual(converted, {
            'id': '1234',
            'data': 'data',
            'data2': 'data2'
        })

    @mock.patch('crm_api.repositories.mongodb.utils.mongoengine.connect')
    def test_connect(self, mock_connect):
        mock_connect.return_value = 'connection'
        connection = utils.connect(
            'db', 'user', 'pass', host='host', port=1234, authentication_source='admin')
        self.assertEqual(connection, 'connection')
        mock_connect.assert_called_once_with(
            db='db', username='user', password='pass',
            host='host', port=1234, authentication_source='admin'
        )
