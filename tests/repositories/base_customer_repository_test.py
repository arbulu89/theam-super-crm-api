"""
Unitary tests for crm_api/repositories/customer_repository.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-11
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.repositories import customer_repository

class TestCustomerRepository(unittest.TestCase):
    """
    Unitary tests for CustomerRepository.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        self._repo = customer_repository.BaseRepository()

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    def test_get_customers(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.get_customers()
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_get_customer_by_id(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.get_customer_by_id('id')
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_create_customer(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.create_customer('name', 'surname', 'user', 'photo')
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_update_customer(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.update_customer('id', 'name', 'surname', 'user', 'photo')
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))

    def test_delete_customer(self):
        with self.assertRaises(NotImplementedError) as err:
            self._repo.delete_customer('id')
            self.assertTrue(
                'must be implemented in specific repository' in str(err.exception))
