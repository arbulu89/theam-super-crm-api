"""
Unitary tests for crm_api/controllers/utils.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-12
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock

from crm_api.controllers import utils

class TestCustomerRepository(unittest.TestCase):
    """
    Unitary tests for CustomerRepository.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.controllers.utils.jwt')
    def test_encode_token(self, mock_jwt):
        mock_jwt.encode.return_value = 'encoded_token'
        encoded_token = utils.encode_token('token')
        self.assertEqual(encoded_token, 'encoded_token')
        mock_jwt.encode.assert_called_once_with(
            {'access_token': 'token'}, utils.SECRET_KEY, algorithm='HS256')

    def test_format_token(self):
        formatted_token = utils.format_token('token')
        self.assertEqual(formatted_token, 'Bearer token')

    def test_get_token(self):
        mock_request = mock.Mock()
        mock_request.headers = {'Authorization': 'Bearer token'}
        token = utils.get_token(mock_request)
        self.assertEqual(token, 'token')

    @mock.patch('crm_api.controllers.utils.abort')
    def test_get_token_malformed(self, mock_abort):
        mock_request = mock.Mock()
        mock_request.headers = {'Authorization': 'token'}
        utils.get_token(mock_request)
        mock_abort.assert_called_once_with(401, message='malformed token')

    @mock.patch('crm_api.controllers.utils.get_token')
    @mock.patch('crm_api.controllers.utils.encode_token')
    def test_is_authorized(self, mock_encode, mock_get_token):
        mock_request = mock.Mock()
        mock_request.headers = {'Authorization': 'Bearer token'}
        mock_cache = mock.Mock()
        mock_cache.get.return_value = 'user'
        mock_get_token.return_value = 'token'
        mock_encode.return_value = 'encoded_token'
        user = utils.is_authorized(mock_request, mock_cache)

        self.assertEqual(user, 'user')
        mock_cache.get.assert_called_once_with('encoded_token')
        mock_get_token.assert_called_once_with(mock_request)
        mock_encode.assert_called_once_with('token')

    @mock.patch('crm_api.controllers.utils.abort')
    def test_is_authorized_header_error(self, mock_abort):
        mock_request = mock.Mock()
        mock_request.headers = {}
        mock_cache = mock.Mock()
        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception) as err:
            utils.is_authorized(mock_request, mock_cache)
            mock_abort.assert_called_once_with(err)

    @mock.patch('crm_api.controllers.utils.get_token')
    @mock.patch('crm_api.controllers.utils.encode_token')
    @mock.patch('crm_api.controllers.utils.abort')
    def test_is_authorized_not_logger(self, mock_abort, mock_encode, mock_get_token):
        mock_request = mock.Mock()
        mock_request.headers = {'Authorization': 'Bearer token'}
        mock_cache = mock.Mock()
        mock_cache.get.return_value = None
        mock_get_token.return_value = 'token'
        mock_encode.return_value = 'encoded_token'
        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception) as err:
            utils.is_authorized(mock_request, mock_cache)
            mock_abort.assert_called_once_with(err)

    @mock.patch('crm_api.controllers.utils.get_token')
    @mock.patch('crm_api.controllers.utils.encode_token')
    def test_is_admin(self, mock_encode, mock_get_token):
        mock_request = mock.Mock()
        mock_request.headers = {'Authorization': 'Bearer token'}
        mock_cache = mock.Mock()
        mock_cache.get.return_value = 'user'
        mock_get_token.return_value = 'token'
        mock_encode.return_value = 'encoded_token'
        mock_service = mock.Mock()
        mock_service.get_user_by_username.return_value = {'is_admin': True}
        utils.is_admin(mock_request, mock_cache, mock_service)

        mock_cache.get.assert_called_once_with('encoded_token')
        mock_get_token.assert_called_once_with(mock_request)
        mock_encode.assert_called_once_with('token')
        mock_service.get_user_by_username.assert_called_once_with('user')

    @mock.patch('crm_api.controllers.utils.get_token')
    @mock.patch('crm_api.controllers.utils.encode_token')
    @mock.patch('crm_api.controllers.utils.abort')
    def test_is_admin_not_logger(self, mock_abort, mock_encode, mock_get_token):
        mock_request = mock.Mock()
        mock_request.headers = {'Authorization': 'Bearer token'}
        mock_cache = mock.Mock()
        mock_cache.get.return_value = None
        mock_get_token.return_value = 'token'
        mock_encode.return_value = 'encoded_token'
        mock_service = mock.Mock()

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception) as err:
            utils.is_admin(mock_request, mock_cache, mock_service)
            mock_abort.assert_called_once_with(err)

    @mock.patch('crm_api.controllers.utils.get_token')
    @mock.patch('crm_api.controllers.utils.encode_token')
    @mock.patch('crm_api.controllers.utils.abort')
    def test_is_authorized_not_authorized(self, mock_abort, mock_encode, mock_get_token):
        mock_request = mock.Mock()
        mock_request.headers = {'Authorization': 'Bearer token'}
        mock_cache = mock.Mock()
        mock_cache.get.return_value = 'user'
        mock_get_token.return_value = 'token'
        mock_encode.return_value = 'encoded_token'
        mock_service = mock.Mock()
        mock_service.get_user_by_username.return_value = {'is_admin': False}
        utils.is_admin(mock_request, mock_cache, mock_service)
        mock_abort.assert_called_once_with(403, message='user not authorized')

    @mock.patch('crm_api.controllers.utils.get_token')
    @mock.patch('crm_api.controllers.utils.encode_token')
    @mock.patch('crm_api.controllers.utils.abort')
    def test_is_authorized_not_user(self, mock_abort, mock_encode, mock_get_token):
        mock_request = mock.Mock()
        mock_request.headers = {'Authorization': 'Bearer token'}
        mock_cache = mock.Mock()
        mock_cache.get.return_value = 'user'
        mock_get_token.return_value = 'token'
        mock_encode.return_value = 'encoded_token'
        mock_service = mock.Mock()
        mock_service.get_user_by_username.side_effect = \
            utils.user_service.UserNotExistError('error')
        utils.is_admin(mock_request, mock_cache, mock_service)
        mock_abort.assert_called_once_with(
            401, message='user with that username does not exist')
