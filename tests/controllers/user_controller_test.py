"""
Unitary tests for crm_api/controllers/user_controller.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-12
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock
import app

from crm_api.controllers import user_controller


class TestUserLogout(unittest.TestCase):
    """
    Unitary tests for User.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        test_app = app.main()
        test_app.testing = True
        self.app = test_app.test_client()
        self._mock_service = mock.Mock()
        self._mock_cache = mock.Mock()
        user_controller.User.inject_service(self._mock_service, self._mock_cache)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.request')
    def test_get(self, mock_request, mock_admin, mock_authorized):
        self._mock_service.get_user_by_username.return_value = 'user'
        response = self.app.get('/user/api/v1.0/id')
        self.assertEqual(response.data.strip().strip('"'), 'user')
        self.assertEqual(response.status_code, 200)
        self._mock_service.get_user_by_username.assert_called_once_with('id')
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
        mock_admin.assert_called_once_with(mock_request, self._mock_cache, self._mock_service)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.abort')
    @mock.patch('crm_api.controllers.user_controller.request')
    def test_get_error(self, mock_request, mock_abort, mock_admin, mock_authorized):
        self._mock_service.get_user_by_username.side_effect = \
            user_controller.user_service.UserNotExistError('error')
        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.get('/user/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                404, message="User with id {} doesn't exist".format('id'))

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.request')
    @mock.patch('crm_api.controllers.user_controller.parser')
    def test_put(self, mock_parser, mock_request, mock_admin, mock_authorized):
        mock_authorized.return_value = 'username'
        self._mock_service.update_user.return_value = 'user'
        mock_parser.parse_args.return_value = {
            'username': 'username', 'name': 'name', 'email': 'email', 'is_admin': True}
        response = self.app.put('/user/api/v1.0/id')
        self.assertEqual(response.data.strip().strip('"'), 'user')
        self.assertEqual(response.status_code, 201)
        self._mock_service.update_user.assert_called_once_with(
            'username', 'name', 'email', True)
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
        mock_admin.assert_called_once_with(mock_request, self._mock_cache, self._mock_service)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.abort')
    @mock.patch('crm_api.controllers.user_controller.request')
    @mock.patch('crm_api.controllers.user_controller.parser')
    def test_put_not_user(
            self, mock_parser, mock_request, mock_abort, mock_admin, mock_authorized):

        mock_authorized.return_value = 'username'
        self._mock_service.update_user.side_effect = \
            user_controller.user_service.UserNotExistError('error')
        mock_parser.parse_args.return_value = {
            'username': 'username', 'name': 'name', 'email': 'email', 'is_admin': True}

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.put('/user/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                404, message="User with id {} doesn't exist".format('id'))

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.abort')
    @mock.patch('crm_api.controllers.user_controller.request')
    @mock.patch('crm_api.controllers.user_controller.parser')
    def test_put_invalid(
            self, mock_parser, mock_request, mock_abort, mock_admin, mock_authorized):

        mock_authorized.return_value = 'username'
        self._mock_service.update_user.side_effect = \
            user_controller.user_service.InvalidDataError('error')
        mock_parser.parse_args.return_value = {
            'username': 'username', 'name': 'name', 'email': 'email', 'is_admin': True}

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.put('/user/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                400, message=str('error'))

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.abort')
    @mock.patch('crm_api.controllers.user_controller.request')
    @mock.patch('crm_api.controllers.user_controller.parser')
    def test_put_superadmin(
            self, mock_parser, mock_request, mock_abort, mock_admin, mock_authorized):

        mock_authorized.return_value = 'username'
        self._mock_service.update_user.side_effect = \
            user_controller.user_service.SuperAdminError('error')
        mock_parser.parse_args.return_value = {
            'username': 'username', 'name': 'name', 'email': 'email', 'is_admin': True}

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.put('/user/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                403, message='User {} is a superadmin. cannot be updated'.format('username'))

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.request')
    def test_delete(self, mock_request, mock_admin, mock_authorized):
        response = self.app.delete('/user/api/v1.0/id')
        self.assertEqual(response.data.strip().strip('"'), '')
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
        self._mock_service.delete_user.assert_called_once_with('id')
        self.assertEqual(response.status_code, 204)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.abort')
    @mock.patch('crm_api.controllers.user_controller.request')
    def test_delete_not_user(self, mock_request, mock_abort, mock_admin, mock_authorized):

        self._mock_service.delete_user.side_effect = \
            user_controller.user_service.UserNotExistError('error')

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.delete('/user/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                404, message="User with id {} doesn't exist".format('id'))

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.abort')
    @mock.patch('crm_api.controllers.user_controller.request')
    def test_delete_not_superadmin(self, mock_request, mock_abort, mock_admin, mock_authorized):

        self._mock_service.delete_user.side_effect = \
            user_controller.user_service.SuperAdminError('error')

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.delete('/user/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                404, message="User {} is a superadmin. cannot be deleted".format('id'))


class TestUserListRepository(unittest.TestCase):
    """
    Unitary tests for UserList.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        test_app = app.main()
        test_app.testing = True
        self.app = test_app.test_client()
        self._mock_service = mock.Mock()
        self._mock_cache = mock.Mock()
        user_controller.UserList.inject_service(self._mock_service, self._mock_cache)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.request')
    def test_get(self, mock_request, mock_admin, mock_authorized):
        self._mock_service.get_users.return_value = 'user'
        response = self.app.get('/user/api/v1.0')
        self.assertEqual(response.data.strip().strip('"'), 'user')
        self.assertEqual(response.status_code, 200)
        self._mock_service.get_users.assert_called_once_with()
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.request')
    @mock.patch('crm_api.controllers.user_controller.parser')
    def test_post(self, mock_parser, mock_request, mock_admin, mock_authorized):
        mock_authorized.return_value = 'username'
        self._mock_service.create_user.return_value = 'user'
        mock_parser.parse_args.return_value = {
            'username': 'username', 'name': 'name', 'email': 'email', 'is_admin': True}
        response = self.app.post('/user/api/v1.0')
        self.assertEqual(response.data.strip().strip('"'), 'user')
        self.assertEqual(response.status_code, 201)
        self._mock_service.create_user.assert_called_once_with(
            'username', 'name', 'email', True)
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.utils.is_admin')
    @mock.patch('crm_api.controllers.user_controller.abort')
    @mock.patch('crm_api.controllers.user_controller.request')
    @mock.patch('crm_api.controllers.user_controller.parser')
    def test_post_invalid(
            self, mock_parser, mock_request, mock_abort, mock_admin, mock_authorized):

        mock_authorized.return_value = 'username'
        self._mock_service.create_user.side_effect = \
            user_controller.user_service.InvalidDataError('error')
        mock_parser.parse_args.return_value = {
            'username': 'username', 'name': 'name', 'email': 'email', 'is_admin': True}

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.post('/user/api/v1.0')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                400, message=str('error'))

class TestUserLogoutRepository(unittest.TestCase):
    """
    Unitary tests for UserLogout.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        test_app = app.main()
        test_app.testing = True
        self.app = test_app.test_client()
        self._mock_cache = mock.Mock()
        user_controller.UserLogout.inject_service(self._mock_cache)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.user_controller.request')
    @mock.patch('crm_api.controllers.utils.get_token')
    @mock.patch('crm_api.controllers.utils.encode_token')
    def test_delete(self, mock_encode, mock_get_token, mock_request, mock_authorized):
        mock_get_token.return_value = 'token'
        mock_encode.return_value = 'encoded_token'
        response = self.app.delete('/logout')
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
        self.assertEqual(response.data.strip().strip('"'), 'Succesfully logout')
        self.assertEqual(response.status_code, 200)
        mock_get_token.assert_called_once_with(mock_request)
        mock_encode.assert_called_once_with('token')
        self._mock_cache.delete.assert_called_once_with('encoded_token')
