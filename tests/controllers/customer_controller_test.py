"""
Unitary tests for crm_api/controllers/customer_controller.py.

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-12
"""

# pylint:disable=C0103,C0111,W0212,W0611

import logging
import unittest

import mock
import app

from crm_api.controllers import customer_controller


class TestCustomerRepository(unittest.TestCase):
    """
    Unitary tests for CustomerRepository.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        test_app = app.main()
        test_app.testing = True
        self.app = test_app.test_client()
        self._mock_service = mock.Mock()
        self._mock_cache = mock.Mock()
        customer_controller.Customer.inject_service(self._mock_service, self._mock_cache)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.request')
    def test_get(self, mock_request, mock_authorized):
        self._mock_service.get_customer_by_id.return_value = 'user'
        response = self.app.get('/customer/api/v1.0/id')
        self.assertEqual(response.data.strip().strip('"'), 'user')
        self.assertEqual(response.status_code, 200)
        self._mock_service.get_customer_by_id.assert_called_once_with('id')
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.abort')
    @mock.patch('crm_api.controllers.customer_controller.request')
    def test_get_error(self, mock_request, mock_abort, mock_authorized):
        self._mock_service.get_customer_by_id.side_effect = \
            customer_controller.customer_service.CustomerNotExistError('error')
        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.get('/customer/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                404, message="Customer with id {} doesn't exist".format('id'))

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.request')
    @mock.patch('crm_api.controllers.customer_controller.parser')
    def test_put(self, mock_parser, mock_request, mock_authorized):
        mock_authorized.return_value = 'username'
        self._mock_service.update_customer.return_value = 'user'
        mock_parser.parse_args.return_value = {
            'name': 'name', 'surname': 'surname', 'photo': 'photo'}
        response = self.app.put('/customer/api/v1.0/id')
        self.assertEqual(response.data.strip().strip('"'), 'user')
        self.assertEqual(response.status_code, 201)
        self._mock_service.update_customer.assert_called_once_with(
            'id', 'name', 'surname', 'username', 'photo')
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.abort')
    @mock.patch('crm_api.controllers.customer_controller.request')
    @mock.patch('crm_api.controllers.customer_controller.parser')
    def test_put_not_customer(
            self, mock_parser, mock_request, mock_abort, mock_authorized):

        mock_authorized.return_value = 'username'
        self._mock_service.update_customer.side_effect = \
            customer_controller.customer_service.CustomerNotExistError('error')
        mock_parser.parse_args.return_value = {
            'name': 'name', 'surname': 'surname', 'photo': 'photo'}

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.put('/customer/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                404, message="Customer with id {} doesn't exist".format('id'))

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.abort')
    @mock.patch('crm_api.controllers.customer_controller.request')
    @mock.patch('crm_api.controllers.customer_controller.parser')
    def test_put_invalid(
            self, mock_parser, mock_request, mock_abort, mock_authorized):

        mock_authorized.return_value = 'username'
        self._mock_service.update_customer.side_effect = \
            customer_controller.customer_service.InvalidDataError('error')
        mock_parser.parse_args.return_value = {
            'name': 'name', 'surname': 'surname', 'photo': 'photo'}

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.put('/customer/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                401, message=str('error'))

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.request')
    def test_delete(self, mock_request, mock_authorized):
        response = self.app.delete('/customer/api/v1.0/id')
        self.assertEqual(response.data.strip().strip('"'), '')
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
        self._mock_service.delete_customer.assert_called_once_with('id')
        self.assertEqual(response.status_code, 204)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.abort')
    @mock.patch('crm_api.controllers.customer_controller.request')
    def test_delete_not_customer(self, mock_request, mock_abort, mock_authorized):

        self._mock_service.delete_customer.side_effect = \
            customer_controller.customer_service.CustomerNotExistError('error')

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.delete('/customer/api/v1.0/id')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                404, message="Customer with id {} doesn't exist".format('id'))


class TestCustomerListRepository(unittest.TestCase):
    """
    Unitary tests for CustomerRepository.
    """

    @classmethod
    def setUpClass(cls):
        """
        Global setUp.
        """

        logging.basicConfig(level=logging.INFO)

    def setUp(self):
        """
        Test setUp.
        """
        test_app = app.main()
        test_app.testing = True
        self.app = test_app.test_client()
        self._mock_service = mock.Mock()
        self._mock_cache = mock.Mock()
        customer_controller.CustomerList.inject_service(self._mock_service, self._mock_cache)

    def tearDown(self):
        """
        Test tearDown.
        """

    @classmethod
    def tearDownClass(cls):
        """
        Global tearDown.
        """

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.request')
    def test_get(self, mock_request, mock_authorized):
        self._mock_service.get_customers.return_value = 'user'
        response = self.app.get('/customer/api/v1.0')
        self.assertEqual(response.data.strip().strip('"'), 'user')
        self.assertEqual(response.status_code, 200)
        self._mock_service.get_customers.assert_called_once_with()
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.request')
    @mock.patch('crm_api.controllers.customer_controller.parser')
    def test_post(self, mock_parser, mock_request, mock_authorized):
        mock_authorized.return_value = 'username'
        self._mock_service.create_customer.return_value = 'user'
        mock_parser.parse_args.return_value = {
            'name': 'name', 'surname': 'surname', 'photo': 'photo'}
        response = self.app.post('/customer/api/v1.0')
        self.assertEqual(response.data.strip().strip('"'), 'user')
        self.assertEqual(response.status_code, 201)
        self._mock_service.create_customer.assert_called_once_with(
            'name', 'surname', 'username', 'photo')
        mock_authorized.assert_called_once_with(mock_request, self._mock_cache)

    @mock.patch('crm_api.controllers.utils.is_authorized')
    @mock.patch('crm_api.controllers.customer_controller.abort')
    @mock.patch('crm_api.controllers.customer_controller.request')
    @mock.patch('crm_api.controllers.customer_controller.parser')
    def test_post_invalid(
            self, mock_parser, mock_request, mock_abort, mock_authorized):

        mock_authorized.return_value = 'username'
        self._mock_service.create_customer.side_effect = \
            customer_controller.customer_service.InvalidDataError('error')
        mock_parser.parse_args.return_value = {
            'name': 'name', 'surname': 'surname', 'photo': 'photo'}

        mock_abort.side_effect = Exception()
        with self.assertRaises(Exception):
            self.app.post('/customer/api/v1.0')
            mock_authorized.assert_called_once_with(mock_request, self._mock_cache)
            mock_abort.assert_called_once_with(
                401, message=str('error'))
