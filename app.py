"""
Main application

:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

import logging
import redis
from flask import Flask
from flask_restful import Api

from crm_api.authorization import oauth

from crm_api.repositories.mongodb import customer_repository
from crm_api.services import customer_service
from crm_api.controllers import customer_controller
from crm_api.repositories.mongodb import user_repository
from crm_api.services import user_service
from crm_api.controllers import user_controller

from config import MONGO_HOST, MONGO_PORT, MONGO_DB, MONGO_USER, \
    MONGO_PASS, REDIS_HOST, REDIS_PORT, REDIS_PASS, DEBUG_MODE, \
    SESSION_EXPIRATION_TIME


LOGIN_BASE_URL = '/' #login and auth resources created by logginpass
LOGOUT_BASE_URL = '/logout'
CUSTOMER_BASE_URL = '/customer/api/v1.0'
USER_BASE_URL = '/user/api/v1.0'

def main():
    """
    Main app
    """
    app = Flask(__name__)
    api = Api(app)

    cache = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=0, password=REDIS_PASS)

    app.config.from_pyfile('config.py')

    # Initialize repos
    customer_repo = customer_repository.MongodbRepository()
    customer_repo.connect(
        host=MONGO_HOST, port=MONGO_PORT, database=MONGO_DB,
        username=MONGO_USER, password=MONGO_PASS, authentication_source='admin')
    user_repo = user_repository.MongodbRepository()
    user_repo.connect(
        host=MONGO_HOST, port=MONGO_PORT, database=MONGO_DB,
        username=MONGO_USER, password=MONGO_PASS, authentication_source='admin')

    # Initialize services
    serv = customer_service.CustomerService(customer_repo)
    user_serv = user_service.UserService(user_repo)

    # Initialize oauth
    oauth.Authorization(
        app, cache, user_serv, LOGIN_BASE_URL, SESSION_EXPIRATION_TIME)

    # Initialize controllers
    customer_cont = customer_controller.Customer.inject_service(serv, cache)
    customer_list_cont = customer_controller.CustomerList.inject_service(serv, cache)
    user_cont = user_controller.User.inject_service(user_serv, cache)
    user_logout_cont = user_controller.UserLogout.inject_service(cache)
    user_list_cont = user_controller.UserList.inject_service(user_serv, cache)


    # Add rest api resources
    api.add_resource(customer_cont, CUSTOMER_BASE_URL+'/<customer_id>')
    api.add_resource(customer_list_cont, CUSTOMER_BASE_URL)
    api.add_resource(user_cont, USER_BASE_URL+'/<username>')
    api.add_resource(user_list_cont, USER_BASE_URL)
    api.add_resource(user_logout_cont, LOGOUT_BASE_URL)

    return app

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main().run(host='0.0.0.0', debug=DEBUG_MODE)
