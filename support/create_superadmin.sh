#Create super admin helper script
#!/usr/bin/sh
MONGO_USER=${MONGO_INITDB_ROOT_USERNAME:=root}
MONGO_PASS=${MONGO_INITDB_ROOT_PASSWORD:=root}
MONGO_DB=${MONGO_INITDB_DATABASE:=test}

SUPERADMIN_USERNAME=${SUPERADMIN_USERNAME:=superadmin}
SUPERADMIN_NAME=${SUPERADMIN_NAME:=SuperAdmin}
SUPERADMIN_EMAIL=${SUPERADMIN_EMAIL:=superadmin@theam.com}

mongo_cmd="var superadmin = { username: \"$SUPERADMIN_USERNAME\", name: \"$SUPERADMIN_NAME\", email: \"$SUPERADMIN_EMAIL\", is_admin: true, is_super_admin: true }; db.user.insert(superadmin);"
mongo -u $MONGO_USER -p $MONGO_PASS --authenticationDatabase admin $MONGO_DB --eval "$mongo_cmd"
