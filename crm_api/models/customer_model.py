"""
Customer model

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-06
"""

from mongoengine import Document, StringField, URLField


class Customer(Document):
    '''
    Customer model

    Attributes:
        name (str): Customer name
        surname (str): Customer surname
        photo (url): Customer photo url
        created_by (str): Reference to the user who created the customer entry
        updated_by (str): Reference to the last user modifying the customer

    # TODO: Improvements:
        - Add creation time
        - Add last update time
    '''
    name = StringField(required=True, max_length=200)
    surname = StringField(required=True, max_length=200)
    photo = URLField(max_length=200)
    created_by = StringField(required=True)
    updated_by = StringField()
