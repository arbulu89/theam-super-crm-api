"""
User model

:author: arbulu89
:contact: arbulu89@gmail.de

:since: 2019-05-06
"""

from mongoengine import Document, BooleanField, StringField, EmailField


class User(Document):
    '''
    User model

    Github user format
    {
      "email": "xarbulu@suse.com",
      "name": "Xabier Arbulu Insausti",
      "picture": "https://avatars1.githubusercontent.com/u/36370954?v=4",
      "preferred_username": "arbulu89",
      "profile": "https://github.com/arbulu89",
      "sub": "36370954",
      "updated_at": 1557297616,
      "website": ""
    }

    Attributes:
        is_admin (bool): True if the user is an admin, False otherwise

    '''
    username = StringField(required=True, max_length=200, unique=True)
    name = StringField(required=True, max_length=200)
    email = EmailField(required=True, max_length=200, unique=True)
    is_admin = BooleanField(required=True, default=False, max_length=200)
    is_super_admin = BooleanField(required=True, default=False, max_length=200)
