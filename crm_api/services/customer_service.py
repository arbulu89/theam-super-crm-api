"""
Customer service
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

from crm_api.repositories import customer_repository


class BaseError(Exception):
    """
    Base customer service exception
    """


class CustomerNotExistError(BaseError):
    """
    Customer does not exist error
    """


class InvalidDataError(BaseError):
    """
    Provider data is not valid
    """


class CustomerService(object):
    """
    Customer service layer

    Args:
        db_connector (customer_repository.BaseRepository): Customer repository
            type db connect
    """

    def __init__(self, db_connector):
        self._db_connector = db_connector

    def get_customers(self):
        """
        Get customers

        """
        return self._db_connector.get_customers()

    def get_customer_by_id(self, customer_id):
        """
        Get customer

        Args:
            customer_id (id): Customer id
        """
        try:
            return self._db_connector.get_customer_by_id(customer_id)
        except customer_repository.CustomerNotExistError as err:
            raise CustomerNotExistError(err)

    def create_customer(self, name, surname, created_by, photo=None):
        """
        Create a new customer

        Args:
            name (str): Customer name
            surname (str): Customer surname
            created_by (id): User Id creating the customer
            photo (str, optional): User photo url
        """
        try:
            return self._db_connector.create_customer(name, surname, created_by, photo)
        except customer_repository.InvalidDataError as err:
            raise InvalidDataError(err)

    def update_customer(self, customer_id, name, surname, updated_by, photo):
        """
        Update existing customer

        Args:
            customer_id (id): Customer id
            name (str): Customer name
            surname (str): Customer surname
            updated_by (id): User Id updating the customer
            photo (str): User photo url
        """
        try:
            return self._db_connector.update_customer(
                customer_id, name, surname, updated_by, photo)
        except customer_repository.CustomerNotExistError as err:
            raise CustomerNotExistError(err)
        except customer_repository.InvalidDataError as err:
            raise InvalidDataError(err)

    def delete_customer(self, customer_id):
        """
        Delete existing customer

        Args:
            customer_id (id): Customer id
        """
        try:
            self._db_connector.delete_customer(customer_id)
        except customer_repository.CustomerNotExistError as err:
            raise CustomerNotExistError(err)
