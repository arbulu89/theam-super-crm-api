"""
User service
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

from crm_api.repositories import user_repository


class BaseError(Exception):
    """
    Base user service exception
    """


class UserNotExistError(BaseError):
    """
    User does not exist error
    """


class InvalidDataError(BaseError):
    """
    Provider data is not valid
    """


class SuperAdminError(BaseError):
    """
    Super admin errors
    """


class UserService(object):
    """
    User service layer

    Args:
        db_connector (user_repository.BaseRepository): User repository
            type db connect
    """

    def __init__(self, db_connector):
        self._db_connector = db_connector

    def get_users(self):
        """
        Get users

        """
        return self._db_connector.get_users()

    def get_user_by_username(self, username):
        """
        Get user

        Args:
            username (str): Username
        """
        try:
            return self._db_connector.get_user_by_username(username)
        except user_repository.UserNotExistError as err:
            raise UserNotExistError(err)

    def get_user_by_email(self, email):
        """
        Get user

        Args:
            email (str): Email
        """
        try:
            return self._db_connector.get_user_by_email(email)
        except user_repository.UserNotExistError as err:
            raise UserNotExistError(err)

    def create_user(self, username, name, email, is_admin):
        """
        Create a new user

        Args:
            username (str): Username
            name (str): Name
            email (str): User email
            is_admin (bool): True if the user is an admin
        """
        try:
            return self._db_connector.create_user(username, name, email, is_admin)
        except user_repository.InvalidDataError as err:
            raise InvalidDataError(err)

    def update_user(self, username, name, email, is_admin):
        """
        Update existing user

        Args:
            username (str): Username
            name (str): Name
            email (str): User email
            is_admin (bool): True if the user is an admin
        """
        user = self.get_user_by_username(username)
        if user['is_super_admin']:
            raise SuperAdminError('superadmin users cannot be updated')

        try:
            return self._db_connector.update_user(username, name, email, is_admin)
        except user_repository.UserNotExistError as err:
            raise UserNotExistError(err)
        except user_repository.InvalidDataError as err:
            raise InvalidDataError(err)

    def delete_user(self, username):
        """
        Delete existing user

        Args:
            username (str): Usernam
        """
        user = self.get_user_by_username(username)
        if user['is_super_admin']:
            raise SuperAdminError('superadmin users cannot be deleted')
        try:
            self._db_connector.delete_user(username)
        except user_repository.UserNotExistError as err:
            raise UserNotExistError(err)
