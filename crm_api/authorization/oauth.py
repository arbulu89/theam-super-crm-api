"""
Oauth authorization

:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

from flask import redirect, jsonify
from flask_restful import abort
from authlib.flask.client import OAuth
from loginpass import create_flask_blueprint, GitHub

from crm_api.services.user_service import UserNotExistError
from crm_api.controllers import utils


class AuthorizationError(Exception):
    """
    Error during authorization
    """


class Authorization(object):
    """
    Oauth authorization
    """

    def __init__(self, app, cache, user_service, url_prefix, expiration_time=600):
        self._cache = cache
        self._service = user_service
        self._expiration_time = expiration_time
        oauth = OAuth(app, cache=cache)

        github_bp = create_flask_blueprint(GitHub, oauth, self._handle_authorize)
        app.register_blueprint(github_bp, url_prefix=url_prefix)

    def _handle_authorize(self, remote, token, user_info):
        if token:
            access_token = token['access_token']
            encoded_token = utils.encode_token(access_token)
            self._cache.set(
                encoded_token, user_info['preferred_username'],
                ex=self._expiration_time)
            formatted_token = utils.format_token(access_token)
        if user_info:
            try:
                self._service.get_user_by_username(user_info['preferred_username'])
            except UserNotExistError:
                try:
                    self._service.get_user_by_email(user_info['email'])
                except UserNotExistError:
                    self._service.create_user(
                        username=user_info['preferred_username'],
                        name=user_info['name'],
                        email=user_info['email'],
                        is_admin=False)
                    return jsonify({
                        'message': 'Successfully registered.',
                        'auth_token': formatted_token}), 201

            return jsonify({
                'message': 'Successfully logged.',
                'auth_token': formatted_token}), 200

        abort(401, message='authorization failed')
