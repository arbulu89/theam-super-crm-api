"""
MongoDB utils
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-11
"""

import mongoengine


def mongo_to_dict(mongo_doc):
    """
    Convert mongo document to dictionary. Mainly used to convert the id entry
    """
    converted = {}
    for key, value in mongo_doc.to_mongo().iteritems():
        if key == '_id':
            converted['id'] = str(value)
        else:
            converted[key] = value
    return converted


def connect(database, username, password, **kwargs):
    """
    Connect to the database

    Args:
        database (str): Database name to connect
        username (str): User used to connect to the database
        password (str): Password used to connect to the databse
    """
    host = kwargs.get('host', 'localhost')
    port = kwargs.get('port', 27017)
    source = kwargs.get('authentication_source', 'admin')

    return mongoengine.connect(
        host=host, port=port, db=database, username=username,
        password=password, authentication_source=source)
