"""
Mongodb customer repository
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

import mongoengine

from crm_api.repositories import customer_repository
from crm_api.models import customer_model
from crm_api.repositories.mongodb import utils


class MongodbRepository(customer_repository.BaseRepository):
    """
    MongoDb repository

    # TODO: add host and port options at least

    """

    def __init__(self):
        self._connection = None

    def connect(self, database, username, password, **kwargs):
        """
        Connect to the database

        Args:
            database (str): Database name to connect
            username (str): User used to connect to the database
            password (str): Password used to connect to the databse
        """
        self._connection = utils.connect(database, username, password, **kwargs)

    def disconnect(self):
        """
        Disconnect from database
        """
        self._connection.close()

    def get_customers(self):
        """
        Get customers

        """
        return [utils.mongo_to_dict(cust) for cust in customer_model.Customer.objects]

    def get_customer_by_id(self, customer_id):
        """
        Get customer

        Args:
            customer_id (id): Customer id
        """
        cust = customer_model.Customer.objects(id=customer_id)
        if not cust:
            raise customer_repository.CustomerNotExistError(
                'Customer with id {} does not exist'.format(customer_id))
        return utils.mongo_to_dict(cust[0])

    def create_customer(self, name, surname, created_by, photo=None):
        """
        Create a new customer

        Args:
            name (str): Customer name
            surname (str): Customer surname
            created_by (id): User Id creating the customer
            photo (str, optional): User photo url
        """
        try:
            new_customer = customer_model.Customer(
                name=name, surname=surname, created_by=created_by, photo=photo)
            new_customer.save()
        except mongoengine.errors.ValidationError as err:
            raise customer_repository.InvalidDataError(
                'Provided user data is not valid: {}'.format(err))
        return utils.mongo_to_dict(new_customer)

    def update_customer(self, customer_id, name, surname, updated_by, photo):
        """
        Update existing customer

        Args:
            customer_id (id): Customer id
            name (str): Customer name
            surname (str): Customer surname
            updated_by (id): User Id updating the customer
            photo (str): User photo url
        """
        if not updated_by:
            raise customer_repository.InvalidDataError(
                'Provided user data is not valid: ValidationError (Customer:None)'
                '(Required parameter missing: [\'updated_by\'])')

        cust = customer_model.Customer.objects(id=customer_id)
        if not cust:
            raise customer_repository.CustomerNotExistError(
                'Customer with id {} does not exist'.format(customer_id))
        cust = cust[0]
        cust.name = name
        cust.surname = surname
        cust.updated_by = updated_by
        cust.photo = photo

        try:
            cust.save()
        except mongoengine.errors.ValidationError as err:
            raise customer_repository.InvalidDataError(
                'Provided user data is not valid: {}'.format(err))
        return utils.mongo_to_dict(cust)

    def delete_customer(self, customer_id):
        """
        Delete existing customer

        Args:
            customer_id (id): Customer id
        """
        cust = customer_model.Customer.objects(id=customer_id)
        if not cust:
            raise customer_repository.CustomerNotExistError(
                'Customer with id {} does not exist'.format(customer_id))
        cust[0].delete()
