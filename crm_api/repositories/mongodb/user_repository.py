"""
Mongodb user repository
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-09
"""

import mongoengine
from pymongo import errors as pymongo_errors

from crm_api.repositories import user_repository
from crm_api.models import user_model
from crm_api.repositories.mongodb import utils


class BaseError(Exception):
    """
    Base error
    """


class UserNotExistError(BaseError):
    """
    No user available
    """


class InvalidDataError(BaseError):
    """
    Invalid user data error
    """


class MongodbRepository(user_repository.BaseRepository):
    """
    MongoDb repository

    # TODO: Move connect and disconnect to a generic place
    """

    def __init__(self):
        self._connection = None

    def connect(self, database, username, password, **kwargs):
        """
        Connect to the database

        Args:
            database (str): Database name to connect
            username (str): User used to connect to the database
            password (str): Password used to connect to the databse
        """
        self._connection = utils.connect(database, username, password, **kwargs)

    def disconnect(self):
        """
        Disconnect from database
        """
        self._connection.close()

    def get_users(self):
        """
        Get users

        """
        return [utils.mongo_to_dict(user) for user in user_model.User.objects]

    def get_user_by_username(self, username):
        """
        Get user

        Args:
            username (str): Username
        """
        user = user_model.User.objects(username=username)
        if not user:
            raise user_repository.UserNotExistError(
                'User with username {} does not exist'.format(username))
        return utils.mongo_to_dict(user[0])

    def get_user_by_email(self, email):
        """
        Get user

        Args:
            email (str): Email
        """
        user = user_model.User.objects(email=email)
        if not user:
            raise user_repository.UserNotExistError(
                'User with email {} does not exist'.format(email))
        return utils.mongo_to_dict(user[0])

    def create_user(
            self, username, name, email, is_admin=False, is_super_admin=False):
        """
        Create a new user

        Args:
            username (str): Username
            name (str): Name
            email (str): User email
            is_admin (bool): True if the user is an admin
            is_super_admin (bool): True if the user is a super admin
        """
        try:
            new_user = user_model.User(
                username=username, name=name, email=email,
                is_admin=is_admin, is_super_admin=is_super_admin)
            new_user.save()
        except mongoengine.errors.ValidationError as err:
            raise user_repository.InvalidDataError(
                'Provided user data is not valid: {}'.format(err))
        except (mongoengine.errors.NotUniqueError, pymongo_errors.DuplicateKeyError) as err:
            raise user_repository.InvalidDataError(
                'Provided unique data already exists: {}'.format(err))
        return utils.mongo_to_dict(new_user)

    def update_user(
            self, username, name, email, is_admin=False):
        """
        Update existing user

        Args:
            username (str): Username
            name (str): Name
            email (str): User email
            is_admin (bool): True if the user is an admin
            is_super_admin (bool): True if the user is a super admin
        """
        user = user_model.User.objects(username=username)
        if not user:
            raise user_repository.UserNotExistError(
                'Customer with username {} does not exist'.format(username))
        user = user[0]
        user.username = username
        user.name = name
        user.email = email
        user.is_admin = is_admin
        user.is_super_admin = False

        try:
            user.save()
        except mongoengine.errors.ValidationError as err:
            raise user_repository.InvalidDataError(
                'Provided user data is not valid: {}'.format(err))
        except (mongoengine.errors.NotUniqueError, pymongo_errors.DuplicateKeyError) as err:
            raise user_repository.InvalidDataError(
                'Provided unique data already exists: {}'.format(err))
        return utils.mongo_to_dict(user)

    def delete_user(self, username):
        """
        Delete existing user

        Args:
            username (str): Username
        """
        user = user_model.User.objects(username=username)
        if not user:
            raise user_repository.UserNotExistError(
                'User with username {} does not exist'.format(username))
        user[0].delete()
