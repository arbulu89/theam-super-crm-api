"""
Base customer repository
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""


class BaseError(Exception):
    """
    Base error
    """


class CustomerNotExistError(BaseError):
    """
    No customer available
    """


class InvalidDataError(BaseError):
    """
    Invalid customer data error
    """


class BaseRepository(object):
    """
    Base repository
    """

    def get_customers(self):
        """
        Get customers

        """
        raise NotImplementedError('must be implemented in specific repository')

    def get_customer_by_id(self, customer_id):
        """
        Get customer

        Args:
            customer_id (id): Customer id
        """
        raise NotImplementedError('must be implemented in specific repository')

    def create_customer(self, name, surname, created_by, photo=None):
        """
        Create a new customer

        Args:
            name (str): Customer name
            surname (str): Customer surname
            created_by (id): User Id creating the customer
            photo (str, optional): User photo url
        """
        raise NotImplementedError('must be implemented in specific repository')

    def update_customer(self, customer_id, name, surname, updated_by, photo):
        """
        Update existing customer

        Args:
            customer_id (id): Customer id
            name (str): Customer name
            surname (str): Customer surname
            updated_by (id): User Id updating the customer
            photo (str): User photo url
        """
        raise NotImplementedError('must be implemented in specific repository')

    def delete_customer(self, customer_id):
        """
        Delete existing customer

        Args:
            customer_id (id): Customer id
        """
        raise NotImplementedError('must be implemented in specific repository')
