"""
Base user repository
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-09
"""


class BaseError(Exception):
    """
    Base error
    """


class UserNotExistError(BaseError):
    """
    No user available
    """


class InvalidDataError(BaseError):
    """
    Invalid user data error
    """


class BaseRepository(object):
    """
    Base repository
    """

    def get_users(self):
        """
        Get users

        """
        raise NotImplementedError('must be implemented in specific repository')

    def get_user_by_username(self, username):
        """
        Get user

        Args:
            username (str): Username
        """
        raise NotImplementedError('must be implemented in specific repository')

    def get_user_by_email(self, email):
        """
        Get user by email

        Args:
            email (str): Email
        """
        raise NotImplementedError('must be implemented in specific repository')

    def create_user(
            self, username, name, email, is_admin=False, is_super_admin=False):
        """
        Create a new user

        Args:
            username (str): Username
            name (str): Name
            email (str): User email
            is_admin (bool): True if the user is an admin
            is_super_admin (bool): True if the user is a super admin
        """
        raise NotImplementedError('must be implemented in specific repository')

    def update_user(
            self, username, name, email, is_admin=False):
        """
        Update existing user

        Args:
            username (str): Username
            name (str): Name
            email (str): User email
            is_admin (bool): True if the user is an admin
        """
        raise NotImplementedError('must be implemented in specific repository')

    def delete_user(self, user_id):
        """
        Delete existing user

        Args:
            user_id (id): User id
        """
        raise NotImplementedError('must be implemented in specific repository')
