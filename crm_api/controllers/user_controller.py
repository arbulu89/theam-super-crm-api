"""
User controller
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

from flask import request
from flask_restful import reqparse, abort, Resource

from crm_api.services import user_service
from crm_api.controllers import utils


parser = reqparse.RequestParser()
parser.add_argument('username', type=str, required=True)
parser.add_argument('name', type=str, required=True)
parser.add_argument('email', type=str, required=True)
parser.add_argument('is_admin', type=bool, required=True)


class User(Resource):
    """
    User controller layer

    Args:
        user_service (user_service.UserService): User service
            type db connect
        cache_connector (redis): Cache instance
    """

    @classmethod
    def inject_service(cls, service, cache_connector):
        cls._service = service
        cls._cache_connector = cache_connector
        return cls

    def get(self, username):
        """
        Get users

        """
        utils.is_authorized(request, self._cache_connector)
        utils.is_admin(request, self._cache_connector, self._service)
        try:
            user = self._service.get_user_by_username(username)
        except user_service.UserNotExistError:
            abort(404, message="User with id {} doesn't exist".format(username))
        return user, 200


    def put(self, username):
        """
        Update existing user

        Args:
            username (id): User id
            name (str): User name
            email (str): User email
            is_admin (bool): True if the user is an admin
        """
        utils.is_authorized(request, self._cache_connector)
        utils.is_admin(request, self._cache_connector, self._service)
        args = parser.parse_args()
        username = args['username']
        name = args['name']
        email = args['email']
        is_admin = args['is_admin']
        try:
            update_user = self._service.update_user(
                username, name, email, is_admin)
        except user_service.UserNotExistError:
            abort(404, message="User with id {} doesn't exist".format(username))
        except user_service.InvalidDataError as err:
            abort(400, message=str(err))
        except user_service.SuperAdminError:
            abort(403, message="User {} is a superadmin. cannot be updated".format(username))
        return update_user, 201

    def delete(self, username):
        """
        Delete existing user

        Args:
            username (str): Usernaem
        """
        utils.is_authorized(request, self._cache_connector)
        utils.is_admin(request, self._cache_connector, self._service)
        try:
            self._service.delete_user(username)
        except user_service.UserNotExistError:
            abort(404, message="User with id {} doesn't exist".format(username))
        except user_service.SuperAdminError:
            abort(403, message="User {} is a superadmin. cannot be deleted".format(username))
        return '', 204


class UserList(Resource):
    """
    User list controller layer

    Args:
        user_service (user_service.UserService): User service
            type db connect
        cache_connector (redis): Cache instance
    """

    @classmethod
    def inject_service(cls, service, cache_connector):
        cls._service = service
        cls._cache_connector = cache_connector
        return cls

    def get(self):
        """
        Get users

        """
        utils.is_authorized(request, self._cache_connector)
        utils.is_admin(request, self._cache_connector, self._service)
        return self._service.get_users(), 200

    def post(self):
        """
        Create a new user

        Args:
            username (str): Username
            name (str): User name
            email (str): User email
            is_admin (bool): True if the user is an admin
        """
        utils.is_authorized(request, self._cache_connector)
        utils.is_admin(request, self._cache_connector, self._service)
        args = parser.parse_args()
        username = args.get('username', None)
        name = args.get('name', None)
        email = args.get('email', None)
        is_admin = args.get('is_admin', None)
        try:
            new_user = self._service.create_user(
                username, name, email, is_admin)
        except user_service.InvalidDataError as err:
            abort(400, message=str(err))
        return new_user, 201


class UserLogout(Resource):
    """
    User logout controller layer

    Args:
        cache_connector (redis): Cache instance
    """

    @classmethod
    def inject_service(cls, cache_connector):
        cls._cache_connector = cache_connector
        return cls

    def delete(self):
        """
        Logout user
        """
        utils.is_authorized(request, self._cache_connector)
        token = utils.get_token(request)
        encoded_token = utils.encode_token(token)
        self._cache_connector.delete(encoded_token)
        return 'Succesfully logout', 200
