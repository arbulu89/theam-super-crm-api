"""
Controller utils

:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

from flask_restful import abort
import jwt

from crm_api.services import user_service

from config import TOKEN_ENCRYPT_SECRET_KEY as SECRET_KEY


class UserNotLoggedError(Exception):
    """
    User not logged error
    """


def encode_token(token):
    """
    Encode token. It is used to encode the token before storing in the cache
    to protect the data if the database is compromised
    """
    return jwt.encode({'access_token': token}, SECRET_KEY, algorithm='HS256')


def format_token(token):
    """
    Format the token to : 'Bearer token'
    """
    return 'Bearer {}'.format(token)


def get_token(request):
    """
    Get token from authorization item
    """
    try:
        return request.headers.get('Authorization').split()[1]
    except IndexError:
        abort(401, message='malformed token')


def is_authorized(request, cache):
    """
    Check if the request is authorized
    """
    if 'Authorization' not in request.headers:
        abort(401, message='token not sent in the header')
    token = get_token(request)
    encoded_token = encode_token(token)

    user = cache.get(encoded_token)
    if not user:
        abort(401, message='user is not logged')
    return user


def is_admin(request, cache, service):
    """
    Check if the user is an admin
    """
    token = get_token(request)
    encoded_token = encode_token(token)
    username = cache.get(encoded_token)
    if not username:
        abort(401, message='user not logged')

    try:
        user = service.get_user_by_username(username)
        if not user['is_admin']:
            abort(403, message='user not authorized')
    except user_service.UserNotExistError:
        abort(401, message='user with that username does not exist')
