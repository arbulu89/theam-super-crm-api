"""
Customer controller
:author: Xabier Arbulu
:contact: arbulu89@gmail.com

:since: 2019-05-07
"""

from flask import request
from flask_restful import reqparse, abort, Resource

from crm_api.services import customer_service
from crm_api.controllers import utils


parser = reqparse.RequestParser()
parser.add_argument('name', type=str, required=True)
parser.add_argument('surname', type=str, required=True)
parser.add_argument('photo', type=str)


class Customer(Resource):
    """
    Customer controller layer

    Args:
        customer_service (customer_service.CustomerService): Customer service
            type db connect
        cache_connector (redis): Cache instance
    """

    @classmethod
    def inject_service(cls, service, cache_connector):
        cls._service = service
        cls._cache_connector = cache_connector
        return cls

    def get(self, customer_id):
        """
        Get customers

        """
        utils.is_authorized(request, self._cache_connector)
        try:
            customer = self._service.get_customer_by_id(customer_id)
        except customer_service.CustomerNotExistError:
            abort(404, message="Customer with id {} doesn't exist".format(customer_id))
        return customer, 200


    def put(self, customer_id):
        """
        Update existing customer

        Args:
            customer_id (id): Customer id
            name (str): Customer name
            surname (str): Customer surname
            updated_by (id): User Id updating the customer
            photo (str): User photo url
        """
        username = utils.is_authorized(request, self._cache_connector)
        args = parser.parse_args()
        # Data integritiy check is done in the model
        name = args.get('name', None)
        surname = args.get('surname', None)
        updated_by = username
        photo = args.get('photo', None)
        try:
            update_customer = self._service.update_customer(
                customer_id, name, surname, updated_by, photo)
        except customer_service.CustomerNotExistError:
            abort(404, message="Customer with id {} doesn't exist".format(customer_id))
        except customer_service.InvalidDataError as err:
            abort(401, message=str(err))
        return update_customer, 201

    def delete(self, customer_id):
        """
        Delete existing customer

        Args:
            customer_id (id): Customer id
        """
        utils.is_authorized(request, self._cache_connector)
        try:
            self._service.delete_customer(customer_id)
        except customer_service.CustomerNotExistError:
            abort(404, message="Customer with id {} doesn't exist".format(customer_id))
        return '', 204


class CustomerList(Resource):
    """
    Customer list controller layer

    Args:
        customer_service (customer_service.CustomerService): Customer service
            type db connect
        cache_connector (redis): Cache instance
    """

    @classmethod
    def inject_service(cls, service, cache_connector):
        cls._service = service
        cls._cache_connector = cache_connector
        return cls

    def get(self):
        """
        Get customers

        """
        utils.is_authorized(request, self._cache_connector)
        return self._service.get_customers(), 200

    def post(self):
        """
        Create a new customer

        Args:
            name (str): Customer name
            surname (str): Customer surname
            created_by (id): User Id creating the customer
            photo (str, optional): User photo url
        """
        username = utils.is_authorized(request, self._cache_connector)
        args = parser.parse_args()
        # Data integritiy check is done in the model
        name = args.get('name', None)
        surname = args.get('surname', None)
        created_by = username
        photo = args.get('photo', None)

        try:
            new_customer = self._service.create_customer(
                name, surname, created_by, photo)
        except customer_service.InvalidDataError as err:
            abort(401, message=str(err))
        return new_customer, 201
