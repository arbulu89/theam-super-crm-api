# How to contribute

## Development workflow

The project follows the standard gitflow workflow. These are the basic rules:

1. Clone the project
2. `master` branch cannot be updated except a Pull Request. The Pull Requests
should come from `development` or `hotfix` branches.
3. `development` branch is used to develop the project.
4. `feature` branches might be used if the development might have big changes.
5. `hotfix` branches are used when inmediate changes are required in `master`
branch.

Here more information:
https://es.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow


## Run unit test

To merge the Pull Requests all the Unit Test must be passing and the code
coverage cannot reduce from current status.

To run the tests locally:

```
pip install virtualenv
virtualenv theam-virtualenv
source theam-virtualenv/bin/activate
cd thema-super-crm-api
pip install -r requirements.dev.txt
tox # admin right might be needed
```


## Run the project for development

To improve the development process, there is a more optimal way of starting
the project code. In this mode, the update `python` code will be automatically
updated in the server execution.

Follow the next instructions for that:

1. Virtual environment
To have the working environment clean and updated, a virtual environment usage
is recommended. For that (skip virtual env creation if it is already created):
```
pip install virtualenv
virtualenv theam-virtualenv
source theam-virtualenv/bin/activate
cd thema-super-crm-api
pip install -r requirements.txt
pip install -r requirements.dev.txt
```

2. Start the project

Run the next commands (to make the export easy a script could be created).

```
cd thema-super-crm-api
export TOKEN_ENCRYPT_SECRET_KEY=your-secret
export SECRET_KEY=your-secret
export GITHUB_CLIENT_ID=your-secret
export GITHUB_CLIENT_SECRET=your-secret
docker-compose -f docker-compose.dev.yml up
python app.py
```
