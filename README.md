# Super CRM api
The Super CRM api is a set of endpoints to manage a customer database.
The api provides a options to list, create, update and delete customers.

Only authorized users are able to use the api. Users access will be provided
by Github Oauth system, so a Github user is required to create new users for
the system.

Find the Rest API documentation in: [API.md](docs/API.md)

The next chapters explain how the setup the and run the project together with some
othe essential information.

## Getting started
To deploy the system we will need to follow the next instructions:

### 1. Setup the github Oauth api
Follow the instructions in: https://developer.github.com/apps/building-oauth-apps/creating-an-oauth-app/
Store the `client id` and the `client secret` as they are required in the next
step.

### 2. Modify the .env file
An `.env` file is used to get the required information to start the project. The
[.env.exampe](.env.example) should be used as a reference, as all the required
parameters are described there. Modify with your data creating a new `.env` file.

### 3. Install the development environment
To install the development environment run:
```
cd theam-super-crm-api
pip install -r requirements.dev.txt # Admin right might be needed
```

### 4. Deploy the project
In order to deploy the project `docker` daemon must be running on the system. This
differs depending on the running system, so check how to for your system.

After that:
```
cd theam-super-crm-api
docker-compose up --build # Admin right might be needed
```

### 5. Stop the project
To stop and clean the project run:

```
cd theam-super-crm-api
docker-compose rm # Admin right might be needed
```

## Dependencies

List of dependencies are specified in the ["Requirements file"](requirements.txt). Items can be installed using pip:

    pip install -r requirements.txt

In order to install a development environment run the next command too:

    pip install -r requirements.dev.txt

## License

See the [LICENSE](LICENSE.md) file for license rights and limitations.

## Author

Xabier Arbulu Insausti (arbulu89@gmail.com)

## Reviewers

*Pull request* preferred reviewers for this project:
- Xabier Arbulu Insausti (arbulu89@gmail.com)
