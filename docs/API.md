# Rest API documentation
The Super CRM api provides the next endpoints.

# Authorization
In order to authorize the api calls a session token must be inserted in the header
of the all requests (except the `login` endpoint). The header must have the next entry:
```
{ "Authorization": "Bearer token_str"}
```

To get the token the new user must be logged in the system. Here
the logging endpoints:

### Login
#### Url
```
/login
```
#### Method
```
GET
```
### Success response
```
{ "message": "Successfully logged."", "auth_token" : "Bearer token_str" }
```

### Logout
#### Url
```
/logout
```
#### Method
```
DELETE
```

## User endpoint
The user endpoint is used to manage the user. This endpoint is only available for
admin/superadmin users.
The api provides the next options:

### Get all users
#### Url
```
/user/api/v1.0
```
#### Method
```
GET
```

### Get user
#### Url
```
/user/api/v1.0/:username
```
#### Method
```
GET
```

### Update user
#### Url
```
/user/api/v1.0/:username
```
#### Method
```
PUT
```
#### Data params
```
username (string, unique): New user username
name (string): New user name
email (string, unique): New use email
is_admin (boolean): True if the new user is an admin
```

### Delete user
#### Url
```
/user/api/v1.0/:username
```
#### Method
```
DELETE
```

### Create a new user
#### Url
```
/user/api/v1.0
```
#### Method
```
POST
```
#### Data params
```
username (string, unique): New user username
name (string): New user name
email (string, unique): New use email
is_admin (boolean): True if the new user is an admin
```

## Customer endpoint
The customer endpoint is used to manage the customers. This endpoint is available
for all the users.
The api provides the next options:

### Get all customers
#### Url
```
/customer/api/v1.0
```
#### Method
```
GET
```

### Get customer
#### Url
```
/customer/api/v1.0/:id
```
#### Method
```
GET
```

### Update customer
#### Url
```
/customer/api/v1.0/:id
```
#### Method
```
PUT
```
#### Data params
```
name (string): New customer username
surname (string): New customer name
photo (url): Customer photo url
```

### Delete customer
#### Url
```
/customer/api/v1.0/:id
```
#### Method
```
DELETE
```

### Create a new customer
#### Url
```
/customer/api/v1.0
```
#### Method
```
POST
```
#### Data params
```
name (string): New customer username
surname (string): New customer name
photo (url): Customer photo url
```
